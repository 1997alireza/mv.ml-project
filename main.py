from utils import load_dataset
from processing import evaluate


if __name__ == '__main__':
    X_train, y_train, X_test, y_test = load_dataset()
    evaluate(X_train, y_train, X_test, y_test)
