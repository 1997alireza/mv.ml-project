from nltk.stem import WordNetLemmatizer
from nltk.corpus import stopwords
from sklearn.feature_extraction.text import TfidfVectorizer
import re
import numpy as np


def preprocess_text(text):
    stemmer = WordNetLemmatizer()

    # Remove all the special characters
    text = re.sub(r'\W', ' ', str(text))

    # Remove all single characters
    text = re.sub(r'\s+[a-zA-Z]\s+', ' ', text)

    # Substituting multiple spaces with single space
    text = re.sub(r'\s+', ' ', text, flags=re.I)

    # Converting to Lowercase
    text = text.lower()

    # Lemmatization
    text = text.split()
    text = [stemmer.lemmatize(word) for word in text]
    text = ' '.join(text)

    return text


def tfidf_vectorizer(documents, max_features=3, min_df=5, max_df=0.7):
    vectorizer = TfidfVectorizer(
        max_features=max_features, min_df=min_df, max_df=max_df, stop_words=stopwords.words('english'))
    vectorizer.fit(documents)
    return vectorizer


def prepare_data(X_train, X_test):
    """
    mapping each data to a vector representing its title, author, and text
    """
    for x in X_train:
        for i in range(len(x)):
            x[i] = preprocess_text(x[i])

    for x in X_test:
        for i in range(len(x)):
            x[i] = preprocess_text(x[i])

    title_vectorizer = tfidf_vectorizer(X_train[:, 0])
    author_vectorizer = tfidf_vectorizer(X_train[:, 1], min_df=10)
    text_vectorizer = tfidf_vectorizer(X_train[:, 2])

    features_train = np.hstack((
        title_vectorizer.transform(X_train[:, 0]).toarray(),
        author_vectorizer.transform(X_train[:, 1]).toarray(),
        text_vectorizer.transform(X_train[:, 2]).toarray()))

    features_test = np.hstack((
        title_vectorizer.transform(X_test[:, 0]).toarray(),
        author_vectorizer.transform(X_test[:, 1]).toarray(),
        text_vectorizer.transform(X_test[:, 2]).toarray()))

    return features_train, features_test
