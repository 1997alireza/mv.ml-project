import pandas as pd
import numpy as np


def load_dataset():
    data = pd.read_csv('./datasets/fake_news/train.csv')
    data = data.drop(['id'], axis='columns')
    X_train = np.array(data.values[:, :-1])
    y_train = np.array(data.values[:, -1], dtype=int)

    data = pd.read_csv('./datasets/fake_news/test.csv')
    data = data.drop(['id'], axis='columns')
    X_test = np.array(data.values)

    data = pd.read_csv('./datasets/fake_news/labels.csv')
    data = data.drop(['id'], axis='columns')
    y_test = np.array(data.values[:, 0], dtype=int)

    return X_train, y_train, X_test, y_test
