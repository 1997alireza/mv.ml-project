from preprocessing import prepare_data
from modelling import TextClassifier
from sklearn.metrics import classification_report, confusion_matrix


def evaluate(X_train, y_train, X_test, y_test):
    features_train, features_test = prepare_data(X_train, X_test)

    print("Linear SVM:")
    classifier = TextClassifier('linear_svm')
    classifier.train(features_train, y_train)

    y_pred = classifier.predict(features_test)

    print(classification_report(y_test, y_pred))

    print("SVM:")
    classifier = TextClassifier('svm')
    classifier.train(features_train, y_train)

    y_pred = classifier.predict(features_test)

    print(classification_report(y_test, y_pred))
