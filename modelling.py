from sklearn import svm
from sklearn.model_selection import GridSearchCV


class TextClassifier:
    def __init__(self, type='svm'):
        self._type = type
        self._model = None

    def train(self, X, y):
        if self._type == 'svm':
            estimator = svm.SVC(kernel="rbf", class_weight='balanced')
            param_grid = {'C': [0.1, 1], 'gamma': [0.01, 0.1, 1]}
            model = GridSearchCV(estimator, param_grid, cv=2)
            model.fit(X, y)
            print("optimum hyperparameters: ", model.best_params_)
            print("training error: ", model.score(X, y))
            self._model = model

        elif self._type == 'linear_svm':
            estimator = svm.SVC(kernel="linear", class_weight='balanced')
            param_grid = {'C': [0.1, 1]}
            model = GridSearchCV(estimator, param_grid, cv=2)
            model.fit(X, y)
            print("optimum hyperparameters: ", model.best_params_)
            print("training error: ", model.score(X, y))
            self._model = model

        else:
            raise Exception('model type not found')

    def predict(self, X):
        return self._model.predict(X)

