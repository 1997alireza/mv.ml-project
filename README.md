### The project includes three main parts:

1) **Preprocessing**
   - Takes the raw data and transforms it to clean numerical data prepared to be processed.
   - The following steps have been done to preprocess the input text data:
     - Removing all the special characters
     - Removing all single characters
     - Substituting multiple spaces with single space
     - Converting to lowercase
     - Stemming and lemmatization
     - Transforming to TF-IDF features
     - Concatenating features of title, author, and subject

2) **Modelling**
   - To model the data the following models have been used and compared:
     - Linear SVM
     - RBF kernel SVM
   - Optimum hyperparameter `Gamma` for RBF kernel SVM and `C` for both models are chosen according to a grid search with 2-fold cross validation.
   - Since data is imbalanced, class weights are used to weigh more for the minority class.

3) **Processing**
   - Processing.py is where the preprocessing and modelling part are connected. Here, the data is preprocessed and models are trained and evaluated.
   - The metrics used for evaluation are as follows:
     - Accuracy
     - Precision
     - Recall
     - F1-Score
   - To identify overfitting, training accuracy and test accuracy are compared.


* After facing overfitting problem, the following steps are done to prevent it:
  - Removing features: It turned out that TF_IDF features with the hyperparameter max_features=3 lead to less overfitting.
  - Less complex model: Instead of RBF kernel SVM, a linear SVM is used. As the results show, the linear SVM has achieved a better test score.
  - Regularization: Using lower values for the hyperparamter `C` which is the regularization parameter.

* Next steps to improve the results:
  - Checking if author and title features are really helpful.
  - Using other techniques rather than TF-IDF such as Word2Vec and GloVe.

---
### Results
Linear SVM:

optimum hyperparameters:  {'C': 1}

training error:  0.7998076923076923

                precision   recall   f1-score   support

           0       0.85      0.58      0.69      2339
           1       0.73      0.92      0.81      2861

    accuracy                           0.76      5200
    macro avg      0.79      0.75      0.75      5200
    weighted avg   0.78      0.76      0.75      5200

SVM:

optimum hyperparameters:  {'C': 0.1, 'gamma': 1}

training error:  0.8264903846153846

                precision   recall   f1-score   support

           0       0.73      0.63      0.68      2339
           1       0.73      0.81      0.77      2861

    accuracy                           0.73      5200
    macro avg      0.73      0.72      0.72      5200
    weighted avg   0.73      0.73      0.73      5200